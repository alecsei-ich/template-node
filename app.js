var express = require('express');
var sys     = require('sys');

(function () {
  'use strict';

  console.log ('start app');

  var app = express();

  app.get('/', function (req, res) {
    res.send('Hello World nyaa!');
  });

  var server = app.listen(3000, function () {

    var host = server.address().address;
    var port = server.address().port;

    console.log('Example app listening at http://%s:%s', host, port);
  });

  process.on('SIGINT', function () {
    console.log('application stop');
    return process.exit(1);
  });

  process.on('exit', function (code) {
    return console.log('application exit', code);
  });

  process.on('uncaughtException', function (err) {
    console.log('application exception', err);
    return process.exit(1);
  });

})();
